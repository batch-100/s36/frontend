import { useState, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import UserContext from "../../UserContext";
import usersData from "../../data/usersdata";
import Router from "next/router";

export default function index() {
  // const [user, setUser] = useState('');
  const { setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function authenticate(e) {
    //prevent redirection via form submission
    e.preventDefault();

      setEmail('');
      setPassword('');

      fetch('http://localhost:4000/api/users/login', {
        method: 'POST',
        
        headers: {
          'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res => res.json()) //kinonvert ulit paputnang json format
      .then(data => {
        console.log(data); //para makita ung binabalik na data
        localStorage.setItem('token', data.accessToken);

        if(data !== null) {
          fetch('http://localhost:4000/api/users/details', {
            headers: {
              Authorization: `Bearer ${data.accessToken}`
            }
          })
          .then(res => res.json())
          .then(data => {
            console.log(data);
            localStorage.setItem('id', data._id)
            //ung id papasahan ng data._id
            localStorage.setItem('isAdmin', data.isAdmin)
            //ung isAdmin papasahan ng data.isAdmin
            Router.push('/courses')


            //changes the value of the user state from app.js
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
            alert('Login successful');
          })
        } else {
          alert('Unsuccessul login.')
        }
      })

    }

    ////////////////////////////////////////////////////
  //   if (email == "" || password == "") {
  //     alert("please input your email and/or password");
  //   } else {
  //     fetch("http://localhost:4000/api/users/login", {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify({
  //         email: email,
  //         password: password,
  //       }),
  //     })
  //       .then((res => {
  //         return res.json();
  //       })
  //       .then((data => {
  //         if (data.accessToken) {
  //           console.log(data);

  //           localStorage.setItem("token", data.accessToken);
  //           fetch("http://localhost:4000/api/users/details", {
  //             headers: {
  //               Authorization: `Bearer ${data.accessToken}`,
  //             },
  //           })
  //             .then((res => {
  //               return res.json();
  //             })
  //             .then((data => {
  //               localStorage.setItem("email", email);
  //               localStorage.setItem("isAdmin", data.isAdmin);
  //               Router.push("/courses");
  //             });
  //         } else {
  //           //authentication failure
  //           alert("Something went wrong!");
  //           Router.push("/register");
  //         }
  //       })
  //   }
  // }

  return (
    <Form onSubmit={(e) => authenticate(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>

      <Button className="bg-primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}
