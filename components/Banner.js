//bootstrap grid system components
import { Jumbotron, Row, Col } from 'react-bootstrap';
//import nextJS Link component for client-side navigation
import Link from 'next/link';
import PropTypes from 'prop-types';

export default function Banner({dataProp}) {
    //destructure the data prop into its properties
    const {title, content, destination, label} = dataProp;
    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{title}</h1>
                    <p>{content}</p>
                    <Link href={destination}><a>{label}</a></Link>
                </Jumbotron>
            </Col>
        </Row>
    )
}

//check that the Banner component is getting the correct prop types
Banner.propTypes = {
    //shape() is used to check that a prop object conforms to a specific "shape"
    data: PropTypes.shape({
        //define the properties and their expected types
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        destination: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
    })
}
